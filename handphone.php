
<?php include 'function/geturl.php';?>
<?php include 'function/auth.php';?>




    <!-- include config -->
    <?php include 'config/database.php';?>


    <?php require 'vendor/autoload.php';?>


    <?php


        // menu
        if(isset($_GET['act'])){

            if($_GET['act']=='create'){

                include 'views/handphone/create.php';
            }
            else if($_GET['act']=='edit'){

                $sql = "SELECT * FROM handphone where id='$_GET[id]'";
                $result = $conn->query($sql);


                include 'views/handphone/edit.php';


            }
            else if($_GET['act']=='show'){

                include 'views/handphone/show.php';

            }
            else if($_GET['act']=='store'){

                
                //validation
                $v = new Valitron\Validator($_POST);
                $v->rule('required', 'merk')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Merk');
                $v->rule('numeric', 'ukuran')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Ukuran');
                $v->rule('numeric', 'baterai')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Baterai');
                $v->rule('numeric', 'harga')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Harga');
                $v->rule('required', 'ukuran')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Ukuran');
                $v->rule('required', 'baterai')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Baterai');
                $v->rule('required', 'harga')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Harga');
                
                if($v->validate()) {

                    if($_FILES["foto"]["name"]!=''){

                        //upload foto
                        $path_parts = pathinfo($_FILES["foto"]["name"]);
                        $extension = $path_parts['extension'];
                        $namefile=rand().'.'.$extension;
                        $target_dir = "assets/uploads/";
                        $target_file = $target_dir . basename($namefile);
                        move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file);

                    }else{

                        $namefile='none.jpg';

                    }

                    

                    $sql = "INSERT INTO handphone VALUES ('', '$_POST[merk]','$_POST[display]','$_POST[rom]','$_POST[ram]','$_POST[kamera]','$_POST[ukuran]','$_POST[baterai]','$_POST[harga]','$_POST[deskripsi]','$namefile')";

                    

                    if ($conn->query($sql) === TRUE) {
                        $status='success';
                    } else {
                        $status='error';
                        echo("Error description: " . mysqli_error($conn));
                    }
                

                }else{

                    $status='error';
                    

                }

                
                include 'views/handphone/create.php';    
                
                
                
            }
            else if($_GET['act']=='update'){

                $id=$_GET['id'];
                $v = new Valitron\Validator($_POST);
                $v->rule('required', 'merk')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Merk');
                $v->rule('numeric', 'ukuran')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Ukuran');
                $v->rule('numeric', 'baterai')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Baterai');
                $v->rule('numeric', 'harga')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Harga');
                $v->rule('required', 'ukuran')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Ukuran');
                $v->rule('required', 'baterai')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Baterai');
                $v->rule('required', 'harga')->message('{field} Kesalaham, Kosong Atau Kesalahan Karakter')->label('Harga');
                

                //validation
                
                if($v->validate()) {
                        //cika poto di edit
                    if($_FILES['foto']['name']!=''){

                        //upload foto
                        $path_parts = pathinfo($_FILES["foto"]["name"]);
                        $extension = $path_parts['extension'];
                        $namefile=rand().'.'.$extension;
                        $target_dir = "assets/uploads/";
                        $target_file = $target_dir . basename($namefile);
                        move_uploaded_file($_FILES["foto"]["tmp_name"], $target_file);
                        $sql = "UPDATE handphone SET foto='$namefile',merk='$_POST[merk]',display='$_POST[display]',rom='$_POST[rom]',ram='$_POST[ram]',kamera='$_POST[kamera]',ukuran='$_POST[ukuran]',baterai='$_POST[baterai]',harga='$_POST[harga]',deskripsi='$_POST[deskripsi]' where id='$id'";

                    }else{

                        $sql = "UPDATE handphone SET merk='$_POST[merk]',display='$_POST[display]',rom='$_POST[rom]',ram='$_POST[ram]',kamera='$_POST[kamera]',ukuran='$_POST[ukuran]',baterai='$_POST[baterai]',harga='$_POST[harga]',deskripsi='$_POST[deskripsi]' where id='$id'";

                    }
                    

                    

                    if ($conn->query($sql) === TRUE) {
                        $status='success';
                    } else {
                        $status='error';
                    }
                

                }else{

                    $status='error';
                    

                }
                $sql = "SELECT * FROM handphone where id='$_GET[id]'";
                $result = $conn->query($sql);
                include 'views/handphone/edit.php';

            }
            else if($_GET['act']=='destroy'){
                
                $id=$_GET['id'];
                $sql = "DELETE FROM handphone WHERE id='$id'";
                
                if ($conn->query($sql) === TRUE) {
                    $status='success';
                } else {
                    $status='error';
                }

                $sql = "SELECT * FROM handphone";
                $result = $conn->query($sql);
                include 'views/handphone/index.php';
            }
            else{
                $sql = "SELECT * FROM handphone";
                $result = $conn->query($sql);
                include 'views/handphone/index.php';
            }
                    
        }else{

            $sql = "SELECT * FROM handphone";
            $result = $conn->query($sql);

            include 'views/handphone/index.php';

        }

        
        
            


    ?>





    

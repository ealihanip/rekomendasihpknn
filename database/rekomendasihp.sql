-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2018 at 08:52 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rekomendasihp`
--

-- --------------------------------------------------------

--
-- Table structure for table `handphone`
--

CREATE TABLE `handphone` (
  `id` int(12) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `display` varchar(50) NOT NULL,
  `rom` int(3) NOT NULL,
  `ram` int(3) NOT NULL,
  `kamera` int(3) NOT NULL,
  `ukuran` float NOT NULL,
  `baterai` int(6) NOT NULL,
  `harga` int(12) NOT NULL,
  `deskripsi` text NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `handphone`
--

INSERT INTO `handphone` (`id`, `merk`, `display`, `rom`, `ram`, `kamera`, `ukuran`, `baterai`, `harga`, `deskripsi`, `foto`) VALUES
(4, 'Samsung Galaxy A6', 'Super AMOLED', 64, 4000, 8, 5.6, 3000, 3799900, '', 'none.jpg'),
(5, 'Samsung Galaxy A6+ (2018)', 'Super AMOLED', 16, 512, 5, 6, 3500, 4899000, '', 'none.jpg'),
(6, 'Samsung Galaxy A8 A800', 'Super AMOLED', 16, 512, 5, 5.7, 3050, 4900000, '', 'none.jpg'),
(7, 'Samsung Galaxy J3 Pro', 'Super AMOLED', 16, 512, 5, 5, 2600, 2499000, '', 'none.jpg'),
(8, 'Samsung Galaxy Alpha', 'Super AMOLED', 16, 512, 5, 4.7, 1860, 3500000, '', 'none.jpg'),
(9, 'Xiaomi Mi 8 Explorer', 'Super AMOLED', 16, 512, 5, 6.21, 3000, 3800000, '', 'none.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `level`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `handphone`
--
ALTER TABLE `handphone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `handphone`
--
ALTER TABLE `handphone`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;



<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="home.php">Home</a>
      </li>

       <li class="nav-item active">
        <a class="nav-link" href="handphone.php">Data Handphone</a>
      </li>

       <li class="nav-item active">
        <a class="nav-link" href="rekomendasihandphone.php">Rekomendasi Handphone (KNN)</a>
      </li>
      
      
    </ul>
   
    <div class="form-inline my-2 my-lg-0">
    <?php if(is_login()==true){?>
      <a class="nav-link" href="login.php?act=logout">Logout</a>
    <?php }else{?>
      
      <a class="nav-link" href="login.php">Login</a>

    <?php }?>
    </div>
  </div>
</nav>


<!-- include part theme -->
<?php  include 'views/includes/header.php';?>

<?php  include 'views/includes/navbarfront.php';?>



<div class='container'>
<div class='row'>
        <div class='col-lg-12'>
            <h1>Rekomendasi Handphone (KNN)</h1>
        </div>

        <div class='col-lg-12'>
            
        <div class="card card-primary">

<?php  include 'views/includes/notification.php';?>

<div class="card-body">
   
            <form method="post" enctype="multipart/form-data" action="rekomendasihandphone.php?act=proses">

        <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header bg-dark">
                <h3 class="card-title">Input Data Uji</h3>
                </div>
                <!-- /.card-header -->
                
                    <div class="card-body">
                        <div class='row'>

                            <div class='col-md-6'>

                                <div class="form-group">
                                    <label for="alamat">Display</label>
                                    <select class="form-control" name="display">
                                        <option value='IPS LCD'>IPS LCD</option>
                                        <option value='AMOLED'>AMOLED</option>
                                        <option value='Super AMOLED'>Super AMOLED</option>
                                        <option value='LTPS IPS LCD'>LTPS IPS LCD</option>
                                        <option value='LED-backlit IPS LCD'>LED-backlit IPS LCD</option>
                                        
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="ukuran">Ukuran</label>
                                        
                                    <div class='input-group'>
                                        <input type="text" class="form-control" id="ukuran" placeholder="Ukuran" name='ukuran'>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Inch</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="alamat">Kamera</label>
                                    <div class='input-group'>
                                    <input type="text" class="form-control" id="kamera" placeholder="kamera" name='kamera'>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Mega Pixel</span>
                                    </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <div class='input-group'>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Rp.</span>
                                        </div>
                                        <input type="text" class="form-control" name='harga' id="namagunung" placeholder="Harga">
                                    </div>
                                    
                                </div>
                            </div>

                            <div class='col-md-6'>
                                <div class="form-group">
                                    <label for="alamat">ROM</label>
                                    <div class='input-group'>
                                    <input type="text" class="form-control" id="rom" placeholder="rom" name='rom'>
                                    <div class="input-group-append">
                                        <span class="input-group-text">GB</span>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="alamat">RAM</label>
                                    <div class='input-group'>
                                    <input type="text" class="form-control" id="ram" placeholder="ram" name='ram'>
                                    <div class="input-group-append">
                                        <span class="input-group-text">GB</span>
                                    </div>
                                    </div>
                                </div>
                                
                            
                                <div class="form-group">
                                    <label for="baterai">Baterai</label>
                                        
                                    <div class='input-group'>
                                        <input type="text" class="form-control" id="baterai" placeholder="Baterai" name='baterai'>
                                        <div class="input-group-append">
                                            <span class="input-group-text">Mah</span>
                                        </div>
                                    </div>
                                </div>
                                

                            </div>

                        </div>
                        
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-dark">PROSES</button>
                    </div>
                        
                </div>
                <!-- /.card -->

                </form>


                </div>
            </div>
        </div>

    </div>

</div>

                 


<?php  include 'views/includes/footer.php';?>
<!-- end include footer part theme -->
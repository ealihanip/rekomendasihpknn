

<!-- include part theme -->
<?php  include 'views/includes/header.php';?>

<?php  include 'views/includes/navbarfront.php';?>



<div class='container'>
    <div class='row'>
        <div class='col-lg-12'>
            <h1></h1>
        </div>

        <div class='col-lg-12'>

        <div class='card'>

                <div class='card-body'>
                    <h3>Data Uji</h3>

                    <div class="table-responsive"><table class='table'>
                        <thead>
                            <tr class='bg-secondary'>

                                <?php foreach($datakolomnilai as $value){?>
                                <th>
                                    <?php echo $value?>
                                </th>

                                <?php }?>

                            </tr>
                        </thead>
                            
                            <tr>

                                <?php foreach($datauji as $value){?>
                                <th>
                                    <?php echo $value?>
                                </th>
                                
                                <?php }?>

                            </tr>
                            
                        <tbody>

                        </tbody>
                    </table></div>


                </div>
            </div>
            <div class='card'>

                <div class='card-body'>
                    <h3>Data Latih</h3>
                                    
                    <div class="table-responsive"><table class='table'>
                        <thead>
                            <tr class='bg-secondary'>

                                <th>

                                    Merk

                                </th>
                                <?php foreach($datakolomnilai as $value){?>
                                <th>
                                    <?php echo $value?>
                                </th>

                                <?php }?>

                            </tr>
                        </thead>
                            <?php foreach($datalatih as $value){?>
                            <tr >

                                <th>

                                    <?php echo $value['merk']?>

                                </th>
                                <?php foreach($datakolomnilai as $key){?>
                                <th>
                                    <?php echo $value['nilai'][$key]?>
                                </th>
                                <?php }?>
                                

                            </tr>
                            <?php }?>
                        <tbody>

                        </tbody>
                    </table></div>


                </div>
            </div>
            <hr>
            <div class='card'>

                <div class='card-body'>
                    <h3>Data Normalisasi</h3>

                    <div class="table-responsive"><table class='table'>
                        <thead>
                            <tr class='bg-secondary'>

                                <th>

                                    Merk

                                </th>
                                <?php foreach($datakolomnilai as $value){?>
                                <th>
                                    <?php echo $value?>
                                </th>

                                <?php }?>

                            </tr>
                        </thead>
                            <?php foreach($datanormalisasi as $value){?>
                            <tr <?php if($value['id']=='datauji'){?>class="table-primary"<?php }?>>

                                <th>

                                    <?php echo $value['merk']?>
                                
                                </th>
                                <?php foreach($datakolomnilai as $key){?>
                                <th>
                                    <?php echo round($value['nilai'][$key],3)?>
                                </th>
                                <?php }?>
                                

                            </tr>
                            <?php }?>
                        <tbody>

                        </tbody>
                    </table></div>


                </div>
            </div>

            <div class='card'>

                <div class='card-body'>
                    <h3>Data Euclidean Distance</h3>

                    <div class="table-responsive"><table class='table'>
                        <thead>
                            <tr class='bg-secondary'>

                                <th>

                                    Merk

                                </th>
                                
                                <th>
                                    Data Euclidean Distance
                                </th>

                                

                            </tr>
                        </thead>
                            <?php foreach($dataeuclidean as $value){?>
                            <tr>

                                <th>

                                    <?php echo $value['merk']?>

                                </th>
                                
                                <th>
                                    <?php echo round($value['nilai'],3)?>
                                </th>
                                
                                

                            </tr>
                            <?php }?>
                        <tbody>

                        </tbody>
                    </table></div>


                </div>
            </div>

            <div class='card'>

                <div class='card-body'>
                    <h3>Hasil Rekomendasi</h3>

                    <div class="table-responsive"><table class='table'>
                        <thead>
                            <tr class='bg-secondary'>

                                <th>

                                    Merk

                                </th>
                                
                                <th>
                                    Data Euclidean Distance
                                </th>

                                

                            </tr>
                        </thead>
                            <?php foreach($dataknn as $value){?>
                            <tr>

                                <th>

                                    <?php echo $value['merk']?>

                                </th>
                                
                                <th>
                                    <?php echo round($value['nilai'],3)?>
                                </th>
                                
                                

                            </tr>
                            <?php }?>
                        <tbody>

                        </tbody>
                    </table></div>


                </div>
            </div>
        </div>

    </div>

</div>

                 


<?php  include 'views/includes/footer.php';?>
<!-- end include footer part theme -->
<!-- include part theme -->
<?php  include 'views/includes/header.php';?>

<?php  include 'views/includes/navbarfront.php';?>



<div class='container'>
    <div class='row'>
        <div class='col-lg-12'>
            <h1>Data Handphone</h1>
        </div>

        <div class='col-lg-12'>
            
            <div class="card card-primary">

                <?php  include 'views/includes/notification.php';?>

                <div class="card-body">
                    <a class='btn btn-dark btn-sm text-white' href='handphone.php?'>Kembali</a>

                    <br>
                    <br>
                    <form method="post" enctype="multipart/form-data" action="handphone.php?act=store">

                        <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header bg-dark">
                        <h3 class="card-title">Tambah Data Handphone</h3>
                        </div>
                        <!-- /.card-header -->
                        
                        <div class="card-body">
                            <div class='row'>

                                <div class='col-md-6'>

                                    <div class="form-group">
                                        <label for="merk">Merk</label>
                                        <input type="text" class="form-control" id="merk" name='merk' placeholder="Merk">
                                    </div>

                                    <div class="form-group">
                                        <label for="alamat">Display</label>
                                        <select class="form-control" name="display">
                                            <option value='IPS LCD'>IPS LCD</option>
                                            <option value='AMOLED'>AMOLED</option>
                                            <option value='Super AMOLED'>Super AMOLED</option>
                                            <option value='LTPS IPS LCD'>LTPS IPS LCD</option>
                                            <option value='LED-backlit IPS LCD'>LED-backlit IPS LCD</option>
                                            
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="ukuran">Ukuran</label>
                                            
                                        <div class='input-group'>
                                            <input type="text" class="form-control" id="ukuran" placeholder="Ukuran" name='ukuran'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Inch</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="alamat">Kamera</label>
                                        <select class="form-control" name="kamera">
                                            <option value='5'>5MP</option>
                                            <option value='8'>8MP</option>
                                            <option value='12'>12MP</option>
                                            <option value='13'>13MP</option>
                                            <option value='16'>16MP</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="keterangan">Deskripsi</label>
                                        <textarea class="form-control" rows="5" name='deskripsi'></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="foto">Foto</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="foto" name='foto' accept="image/*">
                                            <label class="custom-file-label" for="foto">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>

                                <div class='col-md-6'>
                                    <div class="form-group">
                                        <label for="alamat">ROM</label>
                                        <select class="form-control" name="rom">
                                            <option value='16'>16GB</option>
                                            <option value='32'>32GB</option>
                                            <option value='64'>64GB</option>
                                            <option value='128'>128GB</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">RAM</label>
                                        <select class="form-control" name="ram">
                                            <option value='512'>512MB</option>
                                            <option value='1000'>1GB</option>
                                            <option value='2000'>2GB</option>
                                            <option value='4000'>4GB</option>
                                            <option value='8000'>8GB</option>
                                        </select>
                                    </div>
                                    
                                
                                    <div class="form-group">
                                        <label for="baterai">Baterai</label>
                                            
                                        <div class='input-group'>
                                            <input type="text" class="form-control" id="baterai" placeholder="Baterai" name='baterai'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Mah</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="harga">Harga</label>
                                        <div class='input-group'>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp.</span>
                                            </div>
                                            <input type="text" class="form-control" name='harga' id="namagunung" placeholder="Harga">
                                        </div>
                                        
                                    </div>

                                </div>

                            </div>
                            
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-dark">Simpan</button>
                        </div>
                            
                    </div>
                    <!-- /.card -->

                    </form>

                  
                </div>
            </div>
        </div>

    </div>
        

</div>

                 


<?php  include 'views/includes/footer.php';?>
<!-- end include footer part theme -->
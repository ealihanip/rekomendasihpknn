<!-- include part theme -->
<?php  include 'views/includes/header.php';?>

<?php  include 'views/includes/navbarfront.php';?>
<?php  include 'views/includes/notification.php';?>
<?php
    

?>

<div class='container'>
    <div class='row'>
        <div class='col-lg-12'>
            <h1>Data Handphone</h1>
        </div>

        <div class='col-lg-12'>
            
            <div class="card card-primary">
                <div class="card-body">
                <?php if(is_login()==true){?>
                <a class='btn btn-dark btn-sm text-white' href='handphone.php?act=create'>Tambah Data</a>
                    
                    <br>

                    <br>
                <?php }?>
                    
                    <table class='table table-bordered' id='tabel-handphone'> 
                        <thead >
                            <tr class='bg-secondary'>
                            <th>
                                #
                            </th>
                            <th>
                                Merk
                            </th>
                            <th>
                                Display
                            </th>
                            <th>
                                Rom
                            </th>
                            <th>
                                Ram
                            </th>
                            <th>
                                Kamera
                            </th>
                            <th>
                                Ukuran
                            </th>
                            <th>
                                Baterain
                            </th>
                            <th>
                                Harga
                            </th>
                            <?php if(is_login()==true){?>
                            <th>
                                act
                            </th>
                            <?php }?>
                            </tr>
                        </thead>

                        <tbody>
                        <?php foreach($result as $value){?>
                            <tr>
                                <td>
                                    <center><img src='<?php echo geturl('http')?>/assets/uploads/<?php echo $value['foto']?>' widt='100px' height='100px'></center>
                                </td>

                                <td>
                                    <?php echo $value['merk']?>
                                </td>
                                <td>
                                    <?php echo $value['display']?>
                                </td>

                                <td>
                                    <?php echo $value['rom']?>GB
                                </td>

                                <td>
                                    <?php echo $value['ram']?>MB
                                </td>

                                <td>
                                    <?php echo $value['kamera']?>Mp
                                </td>
                                <td>
                                    <?php echo $value['ukuran']?>"
                                </td>
                                <td>
                                    <?php echo $value['baterai']?>Mah
                                </td>

                                <td>
                                    Rp.<?php echo number_format($value['harga'],0,',','.');?>,-
                                </td>
                                <?php if(is_login()==true){?>
                                
                                <td width='100px'>
                                    <a href='handphone.php?act=edit&id=<?php echo $value['id']?>' class='btn btn-warning btn-sm'>Edit</a>
                                    <a href='handphone.php?act=destroy&id=<?php echo $value['id']?>' class='btn btn-danger btn-sm'>Hapus</a>
                                </td>
                                <?php }?>
                            </tr>

                            <?php }?>
                        </tbody>

                    </table>
                    
                </div>
            </div>
        </div>

    </div>
        

</div>

<script>

$(document).ready(function() {
    $('#tabel-handphone').DataTable( {
        "paging":   true,
        "ordering": false,
        "info":     false,
        "lengthChange": false
    } );
} );

</script>


<?php  include 'views/includes/footer.php';?>
<!-- end include footer part theme -->
<!-- include part theme -->
<?php  include 'views/includes/header.php';?>

<?php  include 'views/includes/navbarfront.php';?>



<div class='container'>
    <div class='row'>
        <div class='col-lg-12'>
            <h1>Data Handphone</h1>
        </div>

        <div class='col-lg-12'>
            
            <div class="card card-primary">

                <?php  include 'views/includes/notification.php';?>

                <div class="card-body">
                    <a class='btn btn-dark btn-sm text-white' href='handphone.php?'>Kembali</a>

                    <br>
                    <br>

                    <?php foreach($result as $value){?>

                    <form method="post" enctype="multipart/form-data" action="handphone.php?act=update&id=<?php echo $value['id']?>">

                        <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header bg-dark">
                        <h3 class="card-title">Edit Data Handphone</h3>
                        </div>
                        <!-- /.card-header -->
                        
                        <div class="card-body">
                            <div class='row'>

                                <div class='col-md-6'>

                                    <div class="form-group">
                                        <label for="merk">Merk</label>
                                        <input type="text" class="form-control" id="merk" name='merk' placeholder="Merk" value='<?php echo $value['merk']?>'>
                                    </div>

                                    <div class="form-group">
                                        <label for="alamat">Display</label>
                                        <select class="form-control" name="display">
                                            <option value='IPS LCD' <?php if($value['display']=='IPS LCD'){echo 'selected';}?>>IPS LCD</option>
                                            <option value='AMOLED' <?php if($value['display']=='AMOLED'){echo 'selected';}?>>AMOLED</option>
                                            <option value='Super AMOLED' <?php if($value['display']=='Super AMOLED'){echo 'selected';}?>>Super AMOLED</option>
                                            <option value='LTPS IPS LCD' <?php if($value['display']=='LTPS IPS LCD'){echo 'selected';}?>>LTPS IPS LCD</option>
                                            <option value='LED-backlit IPS LCD' <?php if($value['display']=='LED-backlit IPS LCD'){echo 'selected';}?>>LED-backlit IPS LCD</option>
                                            
                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label for="ukuran">Ukuran</label>
                                            
                                        <div class='input-group'>
                                            <input type="text" class="form-control" id="ukuran" placeholder="Ukuran" name='ukuran' value='<?php echo $value['ukuran']?>'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Inch</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="alamat">Kamera</label>
                                        <select class="form-control" name="kamera">
                                            <option value='5' <?php if($value['kamera']=='5'){echo 'selected';}?>>5MP</option>
                                            <option value='8' <?php if($value['kamera']=='8'){echo 'selected';}?>>8MP</option>
                                            <option value='12' <?php if($value['kamera']=='12'){echo 'selected';}?>>12MP</option>
                                            <option value='13' <?php if($value['kamera']=='13'){echo 'selected';}?>>13MP</option>
                                            <option value='16' <?php if($value['kamera']=='16'){echo 'selected';}?>>16MP</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="keterangan">Deskripsi</label>
                                        <textarea class="form-control" rows="5" name='deskripsi' value='<?php echo $value['deskripsi']?>'></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="foto">Foto</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="foto" name='foto' accept="image/*">
                                            <label class="custom-file-label" for="foto">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                    

                                </div>

                                <div class='col-md-6'>
                                    <div class="form-group">
                                        <label for="alamat">ROM</label>
                                        <select class="form-control" name="rom">
                                            <option value='16' <?php if($value['rom']=='16'){echo 'selected';}?>>16GB</option>
                                            <option value='32' <?php if($value['rom']=='32'){echo 'selected';}?>>32GB</option>
                                            <option value='64' <?php if($value['rom']=='64'){echo 'selected';}?>>64GB</option>
                                            <option value='128' <?php if($value['rom']=='128'){echo 'selected';}?>>128GB</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">RAM</label>
                                        <select class="form-control" name="ram">
                                            <option value='512' <?php if($value['ram']=='512'){echo 'selected';}?>>512MB</option>
                                            <option value='1000' <?php if($value['ram']=='1000'){echo 'selected';}?>>1GB</option>
                                            <option value='2000' <?php if($value['ram']=='2000'){echo 'selected';}?>>2GB</option>
                                            <option value='4000' <?php if($value['ram']=='4000'){echo 'selected';}?>>4GB</option>
                                            <option value='8000' <?php if($value['ram']=='8000'){echo 'selected';}?>>8GB</option>
                                        </select>
                                    </div>
                                    
                                
                                    <div class="form-group">
                                        <label for="baterai">Baterai</label>
                                            
                                        <div class='input-group'>
                                            <input type="text" class="form-control" id="baterai" placeholder="Baterai" name='baterai'  value='<?php echo $value['baterai']?>'>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Mah</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="harga">Harga</label>
                                        <div class='input-group'>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Rp.</span>
                                            </div>
                                            <input type="text" class="form-control" name='harga' id="namagunung" placeholder="Harga"  value='<?php echo $value['harga']?>'>
                                        </div>
                                        
                                    </div>

                                </div>

                            </div>
                            
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-dark">Simpan</button>
                        </div>
                            
                    </div>
                    <!-- /.card -->

                    </form>

                    <?php }?>

                  
                </div>
            </div>
        </div>

    </div>
        

</div>

                 


<?php  include 'views/includes/footer.php';?>
<!-- end include footer part theme -->
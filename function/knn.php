<?php
   
    
    function getkolomnilai(){
        include 'database.php';



        $sql = "SHOW COLUMNS FROM handphone";
        $query = mysqli_query($conn,$sql);


        $result=array();
        while($row = mysqli_fetch_array($query)){
            

            if($row['Field']!='id'&&$row['Field']!='merk'&&$row['Field']!='deskripsi'&&$row['Field']!='foto'){

               

                array_push($result,$row['Field']);



            }

        }

        return $result;

    }

    function getdatahandphone($data){
        include 'database.php';



        $sql = "select * FROM handphone";
        $query = mysqli_query($conn,$sql);


        $result=array();
        while($row = mysqli_fetch_array($query)){
            


            
            foreach($data as $key){
                // echo $row[$key];

               

                    $nilai[$key]=$row[$key];
                
                
                
            }
            
            
           
            
            $array=array(
                    
                'id'=>$row['id'],
                'merk'=>$row['merk'],
                'nilai'=>$nilai
            );

            array_push($result,$array);


        }

        

        return $result;

    }

    function normalisasi($data,$key,$datauji){
        
        $result=array();
        

        foreach($key as $datakey){
            
            $formaxmin[$datakey]=array();
        }
        //tambahkan data latih 
        foreach($data as $value){

            foreach($key as $datakey){
                
                if(is_numeric($value['nilai'][$datakey])==true){
                    
                    array_push($formaxmin[$datakey],$value['nilai'][$datakey]);
                    
                }
                

            }
         
        
        }

        //tambah data uji
        foreach($key as $datakey){
            
            if(is_numeric($datauji[$datakey])==true){
                
                array_push($formaxmin[$datakey],$datauji[$datakey]);
                
            }
            

        }
        

        foreach($data as $value){

            foreach($key as $datakey){
                
                if(is_numeric($value['nilai'][$datakey])==true){
                    

                    if(min($formaxmin[$datakey])!=max($formaxmin[$datakey])){
                        $normalisasi=($value['nilai'][$datakey]-min($formaxmin[$datakey]))/(max($formaxmin[$datakey])-min($formaxmin[$datakey]));
                        
                    }else{

                        $normalisasi=0;
                    }
                    $nilai[$datakey]=$normalisasi;
                    
                }else{

                    if($value['nilai'][$datakey]==$datauji[$datakey]){

                        $normalisasi=0;
                    }else{

                        $normalisasi=1;
                        
                    }
                   
                    $nilai[$datakey]=$normalisasi;
                };

                
                

            }


            $array=array(
                    
                'id'=>$value['id'],
                'merk'=>$value['merk'],
                'nilai'=>$nilai
            );

            array_push($result,$array);
         
        
        }

        //tambah data uji 
       
        foreach($key as $datakey){
            
            if(is_numeric($datauji[$datakey])==true){
                

                if(min($formaxmin[$datakey])!=max($formaxmin[$datakey])){
                    $normalisasi=($datauji[$datakey]-min($formaxmin[$datakey]))/(max($formaxmin[$datakey])-min($formaxmin[$datakey]));
                    
                }else{

                    $normalisasi=0;
                }
                $nilai[$datakey]=$normalisasi;
                
            }else{

                if($datauji[$datakey]==$datauji[$datakey]){

                    $normalisasi=0;
                }else{

                    $normalisasi=1;
                    
                }
                
                $nilai[$datakey]=$normalisasi;
            };

            
            

        }


        $array=array(
                
            'id'=>'datauji',
            'merk'=>'Data Uji',
            'nilai'=>$nilai
        );

        array_push($result,$array);
         

        return $result;

    }

    function euclidean($datanormalisasi,$datakolomnilai){

        $result=array();

        //ambil data uji
        foreach ($datanormalisasi as $data){

            //mengulang kecuali data uji
            if($data['id']=='datauji'){

                foreach($datakolomnilai as $key ){

                    $datauji[$key]=$data['nilai'][$key];
                }
                

            }
        }


        //menghitung Jarak
        foreach ($datanormalisasi as $datatraining){

            //mengulang kecuali data uji
            if($datatraining['id']!='datauji'){
                $euclidean=0;
                foreach($datakolomnilai as $key ){

                    $euclidean +=($datatraining['nilai'][$key]-$datauji[$key])*($datatraining['nilai'][$key]-$datauji[$key]);
                }

                $euclidean=sqrt($euclidean);

                $array=array(
                    
                    'id'=>$datatraining['id'],
                    'merk'=>$datatraining['merk'],
                    'nilai'=>$euclidean
                );
        
                array_push($result,$array);
                

            }

           
        }

        

        return $result;
    }

    function knn($data,$k){

        usort($data, function($a, $b) {
            return $a['nilai'] <=> $b['nilai'];
        });

        $data=array_slice($data,0,$k);

        $result=$data;
        return $result;
    }

    




?>